#!/bin/bash
####
mydate=$(date '+%Y%m%d')
ruleid='xccdf_mil.disa.stig_rule_SV-230266r627750_rule'
version='RHEL-08-010372'
#Severity: medium
huntfile='/etc/sysctl.conf' 
huntvalue='kernel.kexec_load_disabled = 1'
log='/root/runccelog'
## this function should only activate if needed, else bail
execute_fx() {  # begin execute
        cp -v ${huntfile}{,.$mydate}
        b4=$(sysctl -p | grep "^kernel.kexec_load_disabled" | grep 1$)
        wget http://10.211.55.66/pub/files/common/r8/sysctl.conf -O /etc/sysctl.conf
        [ -d /etc/etc ] || mkdir /etc/etc/
        wget http://10.211.55.66/pub/files/common/r8/sysctl.conf -O /etc/etc/sysctl.conf
        after=$(sysctl -p | grep "$huntvalue" | grep 1$)
	echo -e "\$b4($b4) \$after($after) $huntfile" 
	echo -e "\$b4($b4) \$after($after) $huntfile" >> $log
        touch $huntfile
### ## sane check goes here
} # end execute_fx

pass() {
echo -e "$ruleid $version pass" >> $log
} # end pass fx

fail() {
echo -e "$ruleid $version failed" >> $log
} # end fail fx

### check for value, run if absent, bail if ok
sysctl -p | egrep kernel.kexec_load_disabled | grep 1$ && echo ok || execute_fx
sysctl -p | egrep "$huntvalue" && pass || fail
#egrep "^$huntvalue" "$huntfile" > /dev/null && echo ok || execute_fx
#egrep "^$huntvalue" "$huntfile" && pass || fail


