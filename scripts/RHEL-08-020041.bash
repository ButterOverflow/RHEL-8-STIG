#!/bin/bash
####
mydate=$(date '+%Y%m%d%H%M%S')
ruleid='xccdf_mil.disa.stig_rule_SV-230349r627750_rule'
version='RHEL-08-020041'
#Severity: medium
huntfile='/etc/bashrc' 
# IS VALUE TYPICALLY PRESENT/Commented out? no/no
# Normal Value=^[ -n "$PS1" -a -z "$TMUX" ] && exec tmux
huntvalue='[ -n "$PS1" -a -z "$TMUX" ] && exec tmux'
rpm -q tmux > /dev/null || yum install tmux && echo tmux already installed
log='/root/runccelog'
## this function should only activate if needed, else bail
execute_fx() {  # begin execute
        cp -v ${huntfile}{,.$mydate}
        b4=$(egrep '"\$TMUX"' $huntfile)
egrep  '^\s*\[\s+-n\s+"\$PS1"\s+-a\s+-z\s+"\$TMUX"\s+\]\s+&&\s+exec\s+tmux\s*(?:#.*)?$' $huntfile || echo "$huntvalue" >> $huntfile
        systemctl daemon-reload
        after=$(egrep '"\$TMUX"' $huntfile)
	echo -e "\$b4($b4) \$after($after) $huntfile" 
	echo -e "\$b4($b4) \$after($after) $huntfile" >>$log
} # end execute_fx

pass() {
echo -e "$ruleid $version pass" >> $log
} # end pass fx

fail() {
echo -e "$ruleid $version failed" >> $log
} # end fail fx

### check for value, run if absent, bail if ok
#egrep "^$huntvalue" "$huntfile" > /dev/null && echo ok || execute_fx
#egrep  '^\s*\[\s+-n\s+"\$PS1"\s+-a\s+-z\s+"\$TMUX"\s+\]\s+&&\s+exec\s+tmux\s*(?:#.*)?$' $huntfile && echo ok || execute_fx
egrep  'TMUX' $huntfile && echo ok || execute_fx
egrep "^$huntvalue" "$huntfile" && pass || fail


