#!/bin/bash
####
mydate=$(date '+%Y%m%d%H%M%S')
ruleid='xccdf_mil.disa.stig_rule_SV-230510r627750_rule'
version='RHEL-08-040022'
huntfile='/etc/fstab' 
huntvalue='tmpfs /dev/shm tmpfs defaults,nodev,nosuid,noexec 0 0'
log='/root/runccelog'
## this function should only activate if needed, else bail
execute_fx() {  # begin execute
        cp -v ${huntfile}{,.$mydate}
        b4=$(grep ^tmpfs $huntfile | grep '/dev/shm')
        egrep "^tmpfs" $huntfile | grep '/dev/shm' | grep nosuid || echo "$huntvalue" >> $huntfile
### it was not previously in there.  Using echo above
##	sed -i "s/^SSH_USE_STRONG_RNG.*/$huntvalue/" $huntfile
##        systemctl daemon-reload
##        systemctl restart sshd
        after=$(grep ^tmpfs $huntfile | grep '/dev/shm' | grep nosuid)
	echo -e "\$b4($b4) \$after($after) $huntfile" 
	echo -e "\$b4($b4) \$after($after) $huntfile" >>$log
### ## sane check goes here
} # end execute_fx

pass() {
echo -e "$ruleid $version pass" >> $log
} # end pass fx

fail() {
echo -e "$ruleid $version failed" >> $log
} # end fail fx

### check for value, run if absent, bail if ok
grep "^tmpfs" "$huntfile" | grep '/dev/shm' | grep nosuid && echo ok || execute_fx
grep "^tmpfs" "$huntfile" | grep '/dev/shm' | grep nosuid && pass || fail


