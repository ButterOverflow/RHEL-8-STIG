#!/bin/bash
####
mydate=$(date '+%Y%m%d%H%M%S')
ruleid='xccdf_mil.disa.stig_rule_SV-230343r743981_rule'
version='RHEL-08-020021'
#Severity: medium
huntfile='/etc/security/faillock.conf' 
# IS VALUE TYPICALLY PRESENT/Commented out? yes/yes
# Normal Value=^# audit
huntvalue='audit'
log='/root/runccelog'
## this function should only activate if needed, else bail
execute_fx() {  # begin execute
##        echo "$huntvalue" >> $huntfile
        cp -v ${huntfile}{,.$mydate}
        b4=$(egrep ^'# audit'$ $huntfile)
	sed -i "s/^# audit$/$huntvalue/" $huntfile
        systemctl daemon-reload
#        systemctl restart nameofservice
        after=$(egrep ^"$huntvalue"$ $huntfile)
	echo -e "\$b4($b4) \$after($after) $huntfile" 
	echo -e "\$b4($b4) \$after($after) $huntfile" >>$log
### ## sane check goes here
} # end execute_fx

pass() {
echo -e "$ruleid $version pass" >> $log
} # end pass fx

fail() {
echo -e "$ruleid $version failed" >> $log
} # end fail fx

### check for value, run if absent, bail if ok
egrep "^$huntvalue"$ "$huntfile" > /dev/null && echo ok || execute_fx
egrep "^$huntvalue"$ "$huntfile" && pass || fail


