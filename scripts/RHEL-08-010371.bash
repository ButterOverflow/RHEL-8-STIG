#!/bin/bash
####
mydate=$(date '+%Y%m%d')
ruleid='xccdf_mil.disa.stig_rule_SV-230265r627750_rule'
version='RHEL-08-010371'
#Severity: high
huntfile='/etc/dnf/dnf.conf' 
huntvalue='localpkg_gpgcheck=True'
log='/root/runccelog'
## this function should only activate if needed, else bail
execute_fx() {  # begin execute
        cp -v ${huntfile}{,.$mydate}
        b4=$(egrep ^localpkg_gpgcheck $huntfile)
        egrep "$huntvalue" $huntfile || echo "$huntvalue" >> $huntfile
### it was not previously in there.  Using echo above
##	sed -i "s/^SSH_USE_STRONG_RNG.*/$huntvalue/" $huntfile
##        systemctl daemon-reload
##        systemctl restart sshd
        after=$(egrep ^$huntvalue $huntfile)
	echo -e "\$b4($b4) \$after($after) $huntfile" 
	echo -e "\$b4($b4) \$after($after) $huntfile" >>$log
### ## sane check goes here
} # end execute_fx

pass() {
echo -e "$ruleid $version pass" >> $log
} # end pass fx

fail() {
echo -e "$ruleid $version failed" >> $log
} # end fail fx

### check for value, run if absent, bail if ok
egrep "^$huntvalue" "$huntfile" > /dev/null && echo ok || execute_fx
egrep "^$huntvalue" "$huntfile" && pass || fail


