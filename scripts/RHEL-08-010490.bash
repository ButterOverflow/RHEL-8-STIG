#!/bin/bash
####
mydate=$(date '+%Y%m%d')
ruleid='xccdf_mil.disa.stig_rule_SV-230287r743951_rule'
version='RHEL-08-010490'
#Severity: medium
huntfile='/etc/rc.d/rc.local' 
huntvalue='chmod 0600 /etc/ssh/ssh_host*key'
log='/root/runccelog'
## this function should only activate if needed, else bail
execute_fx() {  # begin execute
        b4=$(egrep "chmod 0600 /etc/ssh/ssh_host" $huntfile)
        egrep "chmod 0600 /etc/ssh/ssh_host" $huntfile || cp -v ${huntfile}{,.$mydate}
        egrep "chmod 0600 /etc/ssh/ssh_host" $huntfile || echo "$huntvalue" >> $huntfile
        chmod u+x $huntfile
        array=$(find /etc/ssh/ssh_host*key -not -perm 0600)
        for i in $array;do chmod 0600 $i;done
        after=$(ls -l /etc/ssh/ssh_host*key)
	echo -e "\$b4($b4) \$after($after) $huntfile" 
	echo -e "\$b4($b4) \$after($after) $huntfile" >>$log
### ## sane check goes here
} # end execute_fx

pass() {
echo -e "$ruleid $version pass" >> $log
} # end pass fx

fail() {
echo -e "$ruleid $version failed" >> $log
} # end fail fx

### check for value, run if absent, bail if ok
egrep "chmod 0600 /etc/ssh/ssh_host" "$huntfile" > /dev/null && echo ok || execute_fx
egrep "chmod 0600 /etc/ssh/ssh_host" "$huntfile" && pass || fail


