#!/bin/bash
####
mydate=$(date '+%Y%m%d%H%M%S')
ruleid='xccdf_mil.disa.stig_rule_SV-230390r627750_rule'
version='RHEL-08-030040'
#Severity: medium
huntfile='/etc/audit/auditd.conf' 
# IS VALUE TYPICALLY PRESENT/Commented out? no/no
# Normal Value=^disk_error_action = SUSPEND
huntvalue='disk_error_action = HALT' #HALT could be overkill, also SYSLOG or SINGLE or HALT
log='/root/runccelog'
## this function should only activate if needed, else bail
execute_fx() {  # begin execute
##        echo "$huntvalue" >> $huntfile
        cp -v ${huntfile}{,.$mydate}
        b4=$(egrep ^disk_error_action $huntfile)
	sed -i "s/^disk_error_action.*/$huntvalue/" $huntfile
        systemctl daemon-reload
#        systemctl restart nameofservice
        after=$(egrep ^"$huntvalue" $huntfile)
	echo -e "\$b4($b4) \$after($after) $huntfile" 
	echo -e "\$b4($b4) \$after($after) $huntfile" >>$log
### ## sane check goes here
} # end execute_fx

pass() {
echo -e "$ruleid $version pass" >> $log
} # end pass fx

fail() {
echo -e "$ruleid $version failed" >> $log
} # end fail fx

### check for value, run if absent, bail if ok
egrep "^$huntvalue" "$huntfile" > /dev/null && echo ok || execute_fx
egrep "^$huntvalue" "$huntfile" && pass || fail


