#!/bin/bash
####
mydate=$(date '+%Y%m%d')
ruleid='xccdf_mil.disa.stig_rule_SV-230314r627750_rule'
version='RHEL-08-010674'
#Severity: medium
huntfile='/etc/systemd/coredump.conf' 
# IS VALUE TYPICALLY PRESENT/Commented out? no/no
# Normal Value=^#Storage=external
huntvalue='Storage=none'
log='/root/runccelog'
## this function should only activate if needed, else bail
execute_fx() {  # begin execute
##        echo "$huntvalue" >> $huntfile
        cp -v ${huntfile}{,.$mydate}
        b4=$(egrep ^Storage=none $huntfile)
	sed -i "s/^#Storage=external.*/$huntvalue/" $huntfile
        systemctl daemon-reload
#        systemctl restart sshd
        after=$(egrep ^$huntvalue $huntfile)
	echo -e "\$b4($b4) \$after($after) $huntfile" 
	echo -e "\$b4($b4) \$after($after) $huntfile" >>$log
### ## sane check goes here
} # end execute_fx

pass() {
echo -e "$ruleid $version pass" >> $log
} # end pass fx

fail() {
echo -e "$ruleid $version failed" >> $log
} # end fail fx

### check for value, run if absent, bail if ok
egrep "^$huntvalue" "$huntfile" > /dev/null && echo ok || execute_fx
egrep "^$huntvalue" "$huntfile" && pass || fail


