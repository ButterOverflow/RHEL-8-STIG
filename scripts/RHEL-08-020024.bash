#!/bin/bash
####
# consider_poam
mydate=$(date '+%Y%m%d%H%M%S')
ruleid='xccdf_mil.disa.stig_rule_SV-230346r627750_rule'
version='RHEL-08-020024'
#Severity: medium
huntfile='/etc/security/limits.conf' 
###
###
# NOTE: depending on the role of the server, this will break things.
## example, use of foreman on a Red Hat Satellite server, among other things.
###
###
# IS VALUE TYPICALLY PRESENT/Commented out? no/no
# Normal Value=''  IS MISSING
huntvalue='* hard maxlogins 10'
# two methods, check1 and check2
check1=`egrep '^[ \t]*\*[ \t]+hard[ \t]+maxlogins[ \t]+10[ \t]*(?:#.*)?$' /etc/security/limits.conf`
echo -e "\n\t\$check1($check1)"
check2=$(cat /etc/security/limits.conf | grep hard | grep maxlogins | tr -s ' ')
log='/root/runccelog'
## this function should only activate if needed, else bail
execute_fx() {  # begin execute
        cp -v ${huntfile}{,.$mydate}
        b4="noncompliant"
        echo "$huntvalue" >> $huntfile
        after=$(egrep grepfor $huntfile)
	echo -e "\$b4($b4) \$after($after) $huntfile" 
	echo -e "\$b4($b4) \$after($after) $huntfile" >>$log
        mkdir -p /etc/etc/security/
        cp /etc/security/limits.conf /etc/etc/security/
} # end execute_fx

pass() {
echo -e "$ruleid $version pass" >> $log
} # end pass fx

fail() {
echo -e "$ruleid $version failed" >> $log
} # end fail fx

### check for value, run if absent, bail if ok
[[ "$check2" == "" ]] && execute_fx || echo ok
egrep "^$huntvalue" "$huntfile" && pass || fail


