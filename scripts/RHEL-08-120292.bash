#!/bin/bash
####
mydate=$(date '+%Y%m%d')
ruleid='xccdf_mil.disa.stig_rule_SV-230253r627750_rule'
version='RHEL-08-120292'
huntfile='/etc/sysconfig/sshd' 
huntvalue='SSH_USE_STRONG_RNG=32'
log='/root/runccelog'
## this function should only activate if needed, else bail
execute_fx() {  # begin execute
##        echo "$huntvalue" >> $huntfile
        cp -v ${huntfile}{,.$mydate}
        b4=$(egrep ^SSH_USE_STRONG_RNG $huntfile)
	sed -i "s/^SSH_USE_STRONG_RNG.*/$huntvalue/" $huntfile
        systemctl daemon-reload
        systemctl restart sshd
        after=$(egrep ^SSH_USE_STRONG_RNG= $huntfile)
	echo "$b4 $after $huntfile" 
	echo "$b4 $after $huntfile" >>$log
### ## sane check goes here
} # end execute_fx

pass() {
echo -e "$ruleid $version pass" >> $log
} # end pass fx

fail() {
echo -e "$ruleid $version failed" >> $log
} # end fail fx

### check for value, run if absent, bail if ok
egrep "^$huntvalue" "$huntfile" > /dev/null && echo ok || execute_fx
egrep "^$huntvalue" "$huntfile" && pass || fail


