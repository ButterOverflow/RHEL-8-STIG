#!/bin/bash
# this was changed in a more recent stig benchmark
# they fixed the bug I identified
# no longer using this method because they want "99" in the file 
# commenting out the remainder, placing this in the ingested file sysctl.conf 

#####
#mydate=$(date '+%Y%m%d%H%M%S')
#ruleid='xccdf_mil.disa.stig_rule_SV-230546r627750_rule'
#version='RHEL-08-040282'
##Severity: medium
#huntfile='/lib/sysctl.d/10-default-yama-scope.conf' 
## IS VALUE TYPICALLY PRESENT/Commented out? VALUE IS WRONG
## Normal Value=^kernel.yama.ptrace_scope = 0 #WRONG
#huntvalue='kernel.yama.ptrace_scope = 1'
#log='/root/runccelog'
### this function should only activate if needed, else bail
#execute_fx() {  # begin execute
#        #cp -v ${huntfile}{,.$mydate}
#        b4=$(egrep ^kernel.yama $huntfile)
#	sed -i "s/^kernel.yama.ptrace_scope.*/$huntvalue/" $huntfile
#        systemctl daemon-reload
#        sysctl --system
#        after=$(egrep ^"$huntvalue" $huntfile)
#	echo -e "\$b4($b4) \$after($after) $huntfile" 
#	echo -e "\$b4($b4) \$after($after) $huntfile" >>$log
#### ## sane check goes here
#} # end execute_fx
#
#pass() {
#echo -e "$ruleid $version pass" >> $log
#} # end pass fx
#
#fail() {
#echo -e "$ruleid $version failed" >> $log
#} # end fail fx
#
#### check for value, run if absent, bail if ok
#grep "^$huntvalue" "$huntfile" > /dev/null && echo ok || execute_fx
#grep "^$huntvalue" "$huntfile" && pass || fail
#
#
