#!/bin/bash
####
mydate=$(date '+%Y%m%d%H%M%S')
ruleid='xccdf_mil.disa.stig_rule_SV-230311r627750_rule'
version='RHEL-08-040671'
#Severity: medium
huntfile='/lib/sysctl.d/50-coredump.conf' 
# IS VALUE TYPICALLY PRESENT/Commented out? VALUE IS WRONG
# Normal Value=^kernel.core_pattern=blah, blah, blah #WRONG
huntvalue='kernel.core_pattern = |/bin/false'
log='/root/runccelog'
## this function should only activate if needed, else bail
execute_fx() {  # begin execute
        b4=$(grep ^kernel.core_pattern $huntfile)
	sed -i 's#^kernel.core_pattern.*#kernel.core_pattern = |/bin/false#' $huntfile
        systemctl daemon-reload
        sysctl --system > /dev/null 2>&1
        after=$(grep ^"kernel.core_pattern" $huntfile)
	echo -e "\$b4($b4) \$after($after) $huntfile" 
	echo -e "\$b4($b4) \$after($after) $huntfile" >>$log
### ## sane check goes here
} # end execute_fx

pass() {
echo -e "$ruleid $version pass" >> $log
} # end pass fx

fail() {
echo -e "$ruleid $version failed" >> $log
} # end fail fx

### check for value, run if absent, bail if ok
grep "^kernel.core_pattern =" "$huntfile" | grep '|/bin/false$' > /dev/null && echo ok || execute_fx
grep "^kernel.core_pattern =" "$huntfile" | grep '|/bin/false$' > /dev/null && pass || fail


