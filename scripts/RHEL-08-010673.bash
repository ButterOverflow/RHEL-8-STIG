#!/bin/bash
####
mydate=$(date '+%Y%m%d')
ruleid='xccdf_mil.disa.stig_rule_SV-230313r627750_rule'
version='RHEL-08-010673'
#Severity: medium
huntfile='/etc/security/limits.conf' 
huntvalue='* hard core 0'
# two methods, check1 and check2
check1=`egrep '^[ \t]*\*[ \t]+hard[ \t]+core[ \t]+0[ \t]*(?:#.*)?$' /etc/security/limits.conf`
echo -e "\n\t\$check1($check1)"
check2=$(cat /etc/security/limits.conf | grep hard | grep core| tr -s ' ')
log='/root/runccelog'
## this function should only activate if needed, else bail
execute_fx() {  # begin execute
        cp -v ${huntfile}{,.$mydate}
        b4="noncompliant"
        echo "$huntvalue" >> $huntfile
        after=$(egrep grepfor $huntfile)
	echo -e "\$b4($b4) \$after($after) $huntfile" 
	echo -e "\$b4($b4) \$after($after) $huntfile" >>$log
        mkdir -p /etc/etc/security/
        cp /etc/security/limits.conf /etc/etc/security/
} # end execute_fx

pass() {
echo -e "$ruleid $version pass" >> $log
} # end pass fx

fail() {
echo -e "$ruleid $version failed" >> $log
} # end fail fx

### check for value, run if absent, bail if ok
[[ "$check2" == "" ]] && execute_fx || echo ok
egrep "^$huntvalue" "$huntfile" && pass || fail


