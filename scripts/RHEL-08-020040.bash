#!/bin/bash
####
mydate=$(date '+%Y%m%d%H%M%S')
ruleid='xccdf_mil.disa.stig_rule_SV-230384r743987_rule'
version='RHEL-08-020040'
huntfile='/etc/tmux.conf' 
huntvalue='set -g lock-command vlock'
log='/root/runccelog'
## this function should only activate if needed, else bail
execute_fx() {  # begin execute
# i doesn't exist yet
#        cp -v ${huntfile}{,.$mydate}
         b4='noncompliant'
	rpm -q tmux || yum install tmux
	touch $huntfile 
        [ -f "$huntfile" ] &&   egrep "$huntvalue" $huntfile || echo "$huntvalue" >> $huntfile
### it was not previously in there.  Using echo above
##	sed -i "s/^SSH_USE_STRONG_RNG.*/$huntvalue/" $huntfile
##        systemctl daemon-reload
##        systemctl restart sshd
        after=$(egrep ^"$huntvalue" $huntfile)
	echo -e "\$b4($b4) \$after($after) $huntfile" 
	echo -e "\$b4($b4) \$after($after) $huntfile" >>$log
### ## sane check goes here
} # end execute_fx

pass() {
echo -e "$ruleid $version pass" >> $log
} # end pass fx

fail() {
echo -e "$ruleid $version failed" >> $log
} # end fail fx

### check for value, run if absent, bail if ok
[ ! -f $huntfile ] && touch $huntfile && echo created blank $huntfile
egrep "^$huntvalue" "$huntfile" > /dev/null && echo ok || execute_fx
egrep "^$huntvalue" "$huntfile" && pass || fail


