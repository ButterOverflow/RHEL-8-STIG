#!/bin/bash
####
mydate=$(date '+%Y%m%d%H%M%S')
ruleid='xccdf_mil.disa.stig_rule_SV-230350r627750_rule'
version='RHEL-08-020042'
#Severity: low
huntfile='/etc/shells' 
# IS VALUE TYPICALLY PRESENT/Commented out? seems so, kill it with sed
# Normal Value= KILL IF FOUND
what2kill='tmux' # by a glob
huntvalue=''
log='/root/runccelog'
## this function should only activate if needed, else bail
execute_fx() {  # begin execute
        cp -v ${huntfile}{,.$mydate}
        b4=$(egrep -i tmux $huntfile)
	sed -i "s/.*tmux.*/$huntvalue/" $huntfile
        systemctl daemon-reload
        after=$(egrep "$what2kill" $huntfile)
	echo -e "\$b4($b4) \$after($after) $huntfile" 
	echo -e "\$b4($b4) \$after($after) $huntfile" >>$log
### ## sane check goes here
} # end execute_fx

pass() {
echo -e "$ruleid $version pass" >> $log
} # end pass fx

fail() {
echo -e "$ruleid $version failed" >> $log
} # end fail fx

### check for value, run if absent, bail if ok
egrep "$what2kill" "$huntfile" > /dev/null && execute_fx || echo ok
egrep "$what2kill" "$huntfile" && fail || pass


