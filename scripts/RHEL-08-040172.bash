#!/bin/bash
####
mydate=$(date '+%Y%m%d%H%M%S')
ruleid='xccdf_mil.disa.stig_rule_SV-230531r627750_rule'
version='RHEL-08-040172'
#Severity: EDIT
huntfile='/etc/systemd/system.conf' 
# IS VALUE TYPICALLY PRESENT/Commented out? value is normally wrong
# Normal Value=^#CtrlAltDelBurstAction=reboot-force
huntvalue='CtrlAltDelBurstAction=none'
log='/root/runccelog'
## this function should only activate if needed, else bail
execute_fx() {  # begin execute
        cp -v ${huntfile}{,.$mydate}
        b4=$(egrep CtrlAltDelBurstAction $huntfile)
	sed -i "s/^#CtrlAltDelBurstAction=.*/$huntvalue/" $huntfile
        systemctl daemon-reload
        after=$(egrep ^"$huntvalue" $huntfile)
	echo -e "\$b4($b4) \$after($after) $huntfile" 
	echo -e "\$b4($b4) \$after($after) $huntfile" >>$log
### ## sane check goes here
} # end execute_fx

pass() {
echo -e "$ruleid $version pass" >> $log
} # end pass fx

fail() {
echo -e "$ruleid $version failed" >> $log
} # end fail fx

### check for value, run if absent, bail if ok
egrep "^$huntvalue" "$huntfile" > /dev/null && echo ok || execute_fx
egrep "^$huntvalue" "$huntfile" && pass || fail


