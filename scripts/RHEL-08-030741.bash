#!/bin/bash
####
mydate=$(date '+%Y%m%d%H%M%S')
ruleid='xccdf_mil.disa.stig_rule_SV-230485r627750_rule'
version='RHEL-08-030741'
huntfile='/etc/chrony.conf' 
huntvalue='port 0'
log='/root/runccelog'
## this function should only activate if needed, else bail
execute_fx() {  # begin execute
        [ ! -f "$huntfile" ] && touch $huntfile
        cp -v ${huntfile}{,.$mydate}
        b4=$(egrep ^"port 0" $huntfile)
        egrep "$huntvalue" $huntfile || echo "$huntvalue" >> $huntfile
### it was not previously in there.  Using echo above
        systemctl daemon-reload
	systemctl is-active && systemctl restart chronyd
        after=$(egrep ^$huntvalue $huntfile)
	echo -e "\$b4($b4) \$after($after) $huntfile" 
	echo -e "\$b4($b4) \$after($after) $huntfile" >>$log
### ## sane check goes here
} # end execute_fx

pass() {
echo -e "$ruleid $version pass" >> $log
} # end pass fx

fail() {
echo -e "$ruleid $version failed" >> $log
} # end fail fx

### check for value, run if absent, bail if ok
egrep "^$huntvalue" "$huntfile" > /dev/null && echo ok || execute_fx
egrep "^$huntvalue" "$huntfile" && pass || fail


