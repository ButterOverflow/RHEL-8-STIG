
#
# /etc/fstab
# Created by anaconda on Wed Sep  8 02:03:11 2021
#
# Accessible filesystems, by reference, are maintained under '/dev/disk/'.
# See man pages fstab(5), findfs(8), mount(8) and/or blkid(8) for more info.
#
# After editing this file, run 'systemctl daemon-reload' to update systemd
# units generated from this file.
#
/dev/mapper/osdisk-slash /                      xfs     defaults        0 0
UUID=9fd9a6e5-7d82-4069-b66c-bb4a63b602ba /boot xfs     defaults,nodev,nosuid        0 0
/dev/mapper/osdisk-home /home                   xfs     defaults,nodev        0 0
/dev/mapper/osdisk-remaining /remaining         xfs     defaults,nodev        0 0
/dev/mapper/osdisk-tmp  /tmp                    xfs     defaults,nodev,nosuid,noexec        0 0
/dev/mapper/osdisk-var  /var                    xfs     defaults,nodev        0 0
/dev/mapper/osdisk-varlog /var/log              xfs     defaults,nodev,nosuid,noexec        0 0
/dev/mapper/osdisk-varlogaudit /var/log/audit   xfs     defaults,nodev,nosuid,noexec        0 0
/dev/mapper/osdisk-vartmp /var/tmp              xfs     defaults,nodev,nosuid,noexec        0 0
/dev/mapper/rhel-swap   none                    swap    defaults        0 0
## added per RHEL-08-040122 
tmpfs /dev/shm tmpfs defaults,nodev,nosuid,noexec 0 0
