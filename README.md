# RedHat Enterprise Linux STIG

This repository aims to provide as close to 100% STIG compliance for RedHat Enterprise Linux (RHEL) 8 as is practical while also being easily configurable.

This is based off the work of RJ Hinton on the [RedHat forums](https://access.redhat.com/discussions/6844551).

## How to use

This repository is meant to be a neutral source for RHEL STIG and should apply as many rules as is practical. However, a fully STIG'd RHEL 8 system is not very functional. Users are expected to use this repository as a starting point and either fork it or clone and make modifications for their specific use case.

### Kickstart
Using the kickstart file included in this repo one can achieve ~97% compliance. For instructions on using kickstart refer to RedHat's [Performing an advanced RHEL 8 installation](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/performing_an_advanced_rhel_8_installation/performing_an_automated_installation_using_kickstart).

### Manual Configuration
This repository has all the configuration files and audit rules required to achieve ~94% compliance. The individual configurations are much more flexible than the kickstart but they must be added post installation. Automating installation of the configuration files will likely involve scripting and using a tool such as [Packer](https://packer.io) or [Vagrant](https://vagrantup.com).

Eventually this repository may offer a script to automate the installation of the included configuration files, but that isn't currently present.

## Contributions

Pull requests and issues are welcome to improve the usability as well as correct erroneous changes not in compliance. Feel free to open issues with problems faced so they can be addressed.
